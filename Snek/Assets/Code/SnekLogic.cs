﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
//public struct NodeData
//{
//    [SerializeField] private Transform transform;
//    [SerializeField] private SpriteRenderer spriteRend;

//    public void setSpriteRenderer(SpriteRenderer sRend, Sprite sprite, Color color)
//    {
//        spriteRend = sRend;
//        spriteRend.sprite = sprite;
//        spriteRend.color = color;
//    }

//    public Transform getTransform() { return transform; }
//    public SpriteRenderer getspriteRenderer() { return spriteRend; }
//}

internal class Node
{
    internal GameObject nodeObject;
    internal Vector3 lastPos;
    internal Node nextNode;

    public Node(GameObject obj)
    {
        nodeObject = obj;
        nextNode = null;
    }
}

internal class SinlgyLinkedList
{
    internal Node head;

    internal void PushFront(SinlgyLinkedList list, GameObject nodeData)
    {
        Node newNode = new Node(nodeData);
        newNode.nextNode = list.head;
        list.head = newNode;
    }

    internal void PushBack(SinlgyLinkedList list, GameObject nodeData)
    {
        Node newNode = new Node(nodeData);
        if(list.head == null) { list.head = newNode; return; }
        Node lastNode = GetLastNode(list);
        lastNode.nextNode = newNode;
    }

    internal Node GetLastNode(SinlgyLinkedList list)
    {
        Node currentListItem = list.head;
        while(currentListItem.nextNode != null)
        { currentListItem = currentListItem.nextNode; }
        return currentListItem;
    }

    internal void PopFront(SinlgyLinkedList list)
    {
        Node headNode = list.head;
        list.head = headNode.nextNode;
        headNode = null;
    }

    internal void PopBack(SinlgyLinkedList list)
    {
        Node currentListItem = list.head;
        Node previousListItem = null;
        while(currentListItem.nextNode != null)
        {
            previousListItem = currentListItem;
            currentListItem = currentListItem.nextNode;
        }
        previousListItem.nextNode = null;
        currentListItem = null;
    }

    internal void Clear(SinlgyLinkedList list)
    {
        Node currentListItem = list.head;
        Node previousListItem = null;
        while(currentListItem.nextNode != null)
        {
            previousListItem = currentListItem;
            currentListItem = currentListItem.nextNode;
            previousListItem.nextNode = null;
            Object.Destroy(previousListItem.nodeObject);
            previousListItem = null;
        }
        Object.Destroy(currentListItem.nodeObject);
        currentListItem = null;
        list.head = null;
    }

    internal int Count(SinlgyLinkedList list)
    {
        int nodeCount = 0;
        if(list.head != null)
        {
            Node currentListItem = list.head;
            nodeCount++;
            while(currentListItem.nextNode != null)
            {
                currentListItem = currentListItem.nextNode;
                nodeCount++;
            }
        }
        return nodeCount;
    }

    internal Node getNodeInList(SinlgyLinkedList list, int index)
    {
        Node currentListItem = list.head;
        for(int i = 0; i < index; i++)
        {
            if(currentListItem.nextNode != null)
            { currentListItem = currentListItem.nextNode; }
            else { Debug.LogError("List does not contain index: " + index + " |Returning last deetected item at: " + i); return currentListItem; }
        }
        return currentListItem;
    }

    internal void movSnek(SinlgyLinkedList list, Vector3 newPos)
    {
        Node currentListItem = list.head;
        Node previousListItem = null;

        currentListItem.lastPos = currentListItem.nodeObject.transform.position;
        previousListItem = currentListItem;
        currentListItem.nodeObject.transform.position += newPos;

        while (currentListItem.nextNode != null)
        {
            previousListItem = currentListItem;
            currentListItem = currentListItem.nextNode;
            currentListItem.lastPos = currentListItem.nodeObject.transform.position;
            currentListItem.nodeObject.transform.position = previousListItem.lastPos;           
        }
    }

}

public class SnekLogic : MonoBehaviour
{

    private enum moveDirection { UP, DOWN, LEFT, RIGHT, NONE }
    private moveDirection currentMovDir = moveDirection.NONE;

    [SerializeField] private SinlgyLinkedList SnekList = new SinlgyLinkedList();

    [Header("Snek Settings")]
    [SerializeField] private Sprite snekSprite = null;
    [SerializeField] private Color snekColor = Color.white;
    [SerializeField] private Vector3 snekTailScale = Vector3.one;
    [SerializeField] private int startingSnakeTailNum = 12;
    private int currentSnakeTailNum = 0;

    private int testInt = 1;

    private void Start()
    {
        SnekList.PushFront(SnekList, this.gameObject);

        InvokeRepeating("calcMovSnek", 1.0f, 1.0f);

    }

    private void Update()
    {
        if (Input.anyKeyDown) { checkInput(); }
    }

    private GameObject createNodeData()
    {
        GameObject tempObj = new GameObject("snekTail", typeof(SpriteRenderer), typeof(BoxCollider2D));
        SpriteRenderer tempRend = tempObj.GetComponent<SpriteRenderer>();
        BoxCollider2D tempColl = tempObj.GetComponent<BoxCollider2D>();

        tempColl.isTrigger = true;
        tempColl.size = new Vector2(0.9f, 0.9f);
        tempObj.tag = "Player";
        tempRend.sprite = snekSprite;
        tempRend.color = snekColor;
        tempObj.transform.position = SnekList.GetLastNode(SnekList).lastPos;
        tempObj.transform.localScale = snekTailScale;

        return tempObj;
    }

    private void checkInput()
    {
        if(Input.GetKeyDown(KeyCode.W)) { currentMovDir = moveDirection.UP; }
        if (Input.GetKeyDown(KeyCode.A)) { currentMovDir = moveDirection.LEFT; }
        if (Input.GetKeyDown(KeyCode.S)) { currentMovDir = moveDirection.DOWN; }
        if (Input.GetKeyDown(KeyCode.D)) { currentMovDir = moveDirection.RIGHT; }
    }

    private void calcMovSnek()
    {
        Vector3 moveDir = Vector3.zero;

        switch(currentMovDir)
        {
            case moveDirection.UP: moveDir = new Vector3(0,1,0); break;
            case moveDirection.DOWN: moveDir = new Vector3(0, -1, 0); break;
            case moveDirection.LEFT: moveDir = new Vector3(-1, 0, 0); break;
            case moveDirection.RIGHT: moveDir = new Vector3(1, 0, 0); break;
            case moveDirection.NONE: moveDir = Vector3.zero; break;
        }

        if(currentMovDir != moveDirection.NONE)
        {
            SnekList.movSnek(SnekList, moveDir);

            if(currentSnakeTailNum < startingSnakeTailNum)
            { SnekList.PushBack(SnekList, createNodeData()); currentSnakeTailNum++; }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch(collision.tag)
        {
            case "GridTile": Debug.Log("GridTile trigger detected!"); break;
            case "WallTile": killSnek(); Debug.Log("WallTile trigger detected!"); break;
            case "Player": killSnek(); Debug.Log("Player trigger detected!"); break;
            case "Apple": collision.gameObject.SetActive(false); SnekList.PushBack(SnekList, createNodeData()); Debug.Log("Apple trigger detected!"); break;
        }
    }

    private void killSnek()
    {
        SnekList.Clear(SnekList);
    }

}
