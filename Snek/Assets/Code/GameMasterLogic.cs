﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMasterLogic : MonoBehaviour
{
    [SerializeField] Vector2 gridSize = Vector2.zero;

    [SerializeField] GameObject gridTile = null;
    [SerializeField] GameObject WallParent = null;
    [SerializeField] GameObject FieldParent = null;

    [SerializeField] GameObject playerObject = null;
    private GameObject Player = null;
    private Vector3 playerSpawnPos = Vector3.zero;

    [SerializeField] GameObject appleObject = null;
    [SerializeField] private float appleSpawnTime;
    private GameObject Apple = null;
    private Vector3 appleSpawnPos = Vector3.zero;

    private void Awake()
    {
        generateGrid();
        spawnPlayer();
        spawnApple();

        InvokeRepeating("moveApple", appleSpawnTime, appleSpawnTime);
    }

    private void Update()
    {
        if(Player == null)
        { restartGame(); }
    }

    private void generateGrid()
    {
        for(int x = 0; x < gridSize.x; x++)
        {
            for(int y = 0; y < gridSize.y; y++)
            {
                if (y == 0 || y == gridSize.y -1 || x == 0 || x == gridSize.x -1)
                {
                    GameObject tempObjRef = Instantiate(gridTile, new Vector3(transform.position.x + x, transform.position.y - y, 0.0f), Quaternion.identity);
                    tempObjRef.tag = "WallTile";
                    tempObjRef.GetComponent<BoxCollider2D>().size = new Vector3(0.9f, 0.9f, 1);
                    tempObjRef.GetComponent<SpriteRenderer>().color = Color.black;
                    tempObjRef.transform.parent = WallParent.transform;
                }
                else
                {
                    GameObject tempObjRef = Instantiate(gridTile, new Vector3(transform.position.x + x, transform.position.y - y, 0.1f), Quaternion.identity);
                    tempObjRef.transform.parent = FieldParent.transform;
                }
            }
        }
    }

    private void spawnPlayer()
    {
        playerSpawnPos = getRandomSpawnPos();
        Player = Instantiate(playerObject, playerSpawnPos, Quaternion.identity);
    }

    private void spawnApple()
    {
        appleSpawnPos = getRandomSpawnPos();
        if(appleSpawnPos != playerSpawnPos)
        { Apple = Instantiate(appleObject, appleSpawnPos, Quaternion.identity); }
        else { appleSpawnPos = getRandomSpawnPos(); }
    }

    private Vector3 getRandomSpawnPos()
    {
        int randomChild = Random.Range(0, FieldParent.transform.childCount);
        Vector3 childPos = FieldParent.transform.GetChild(randomChild).transform.position;
        Vector3 spawnPos = new Vector3(childPos.x, childPos.y, 0);
        return spawnPos;
    }

    private void moveApple()
    {
        if(Apple.activeInHierarchy == false)
        {
            Debug.Log("Apple not active in Hierarchy!");
            Vector3 movePos = getRandomSpawnPos();
            Ray ray = Camera.main.ScreenPointToRay(movePos);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

            if(hit.collider != null && hit.collider.transform.tag != "Player")
            {
                Debug.Log("Hit nothing, good!");
                Apple.transform.position = movePos;
                Apple.SetActive(true);
            }
        }
    }

    public void restartGame()
    {
        spawnPlayer();
        Apple.SetActive(false);
        moveApple();
    }
}
