﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoLogic : MonoBehaviour
{
    [SerializeField] private float gizmoSize = 0.2f;
    [SerializeField] private Color gizmoColor = Color.white;

    private void OnDrawGizmos()
    {
        Gizmos.color = gizmoColor;
        Gizmos.DrawWireSphere(transform.position, gizmoSize);
    }
}
